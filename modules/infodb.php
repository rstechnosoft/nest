<?php
/**
 * This file is a part of MyWebSQL package
 *
 * @file:      modules/infodb.php
 * @author     Samnan ur Rehman
 * @copyright  (c) 2008-2014 Samnan ur Rehman
 * @web        http://mywebsql.net
 * @license    http://mywebsql.net/license
 */

	function processRequest(&$db) {
            $message = '';
		if (getDbName() == '') {
			echo view('invalid_request');
			return;
		}
                $objects =  $db->getObjectList();
                print "<div id='results'>";
                print "<table cellspacing=\"0\" width='100%' border=\"0\" class='results' id=\"infoTable\"><thead>\n";
                print "<tr id=\"fhead\">";
		print "<th class=\"th index\"><div>#</div></th>";
                print "<th nowrap=\"nowrap\" class='th' data-sort=\"text\"><div>";
		print "TABLE</div></th>";
                print "<th nowrap=\"nowrap\" class='th' data-sort=\"text\"><div>";
		print "ACTION</div></th>";
                $j = 0;
                foreach ($objects['tables'] as $object)
                { 
                     print "</tr></thead><tbody>\n";
                     print "<tr id=\"rcc$j\" class=\"row\">";
                     $j=$j+1;
                     $numRows = $j;
		     print "<td class=\"tj\">".$j."</td>";
                     print "<td nowrap=\"nowrap\" class=\"tl\">$object</td>";
                     print "<td nowrap=\"nowrap\" class=\"tl\">";
                     print "&nbsp;&nbsp;";
                     print "<a href='javascript:tableDescribe(\"$object\")'>Describe</a>"; 
                     print "&nbsp;&nbsp;";
                     print "<a href='javascript:tableAlter(\"$object\")'>AlterStructure</a>";
                     print "&nbsp;&nbsp;";
                     print "<a href='javascript:tableEngine(\"$object\")'>AlterEngine</a>";
                     print "&nbsp;&nbsp;";
                     print "<a href='javascript:tableIndexes(\"$object\")'>AlterIndex</a>";
                     print "&nbsp;&nbsp;";
                     print "<a href='javascript:objTruncate(\"table\", \"$object\")')'>Truncate</a>";
                     print "&nbsp;&nbsp;";
                     print "<a href='javascript:objDrop(\"table\", \"$object\")')'>Drop</a>";
                     print "</td>";
                     print "</tr>";
                 }
                print "</tbody></table>";
		print "</div>";
                $js = "<script type=\"text/javascript\" language=\"javascript\">\n";
		$tm = $db->getQueryTime();
		$js .= "parent.transferInfoMessage();\n";
		$js .= "parent.resetFrame();\n";
		$js .= "</script>\n";
		print $js;
                /*if ($db->queryTableStatus())
			createSimpleGrid($db, __('Database summary').': ['.htmlspecialchars(getDbName()).']');
                 * 
                 */
	}

?>