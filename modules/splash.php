<?php
/**
 * This file is a part of MyWebSQL package
 *
 * @file:      modules/splash.php
 * @author     Samnan ur Rehman
 * @copyright  (c) 2008-2014 Samnan ur Rehman
 * @web        http://mywebsql.net
 * @license    http://mywebsql.net/license
 */

	function getSplashScreen($msg = '', $formCode='') {
		if ($formCode) {
			$formCode = '<div class="login"><form method="post" action="" name="dbform" id="dbform" style="text-align:center">'
							. $formCode . '</form></div>';
		}

		$scripts = "jquery";
		$extraScript = "";
 
		$replace = array(
			'MESSAGE' => $msg ? '<div class="msg">'.htmlspecialchars($msg).'</div>' : '',
			'FORM'    => $formCode,
			'APP_VERSION'  => APP_VERSION,
			'PROJECT_SITEURL' => PROJECT_SITEURL,
			'SCRIPTS' => $scripts,
			'EXTRA_SCRIPT' => $extraScript
			);

		return view('splash', $replace);
	}
?>