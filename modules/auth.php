<?php

/**
 * This file is a part of MyWebSQL package
 *
 * @file:      modules/auth.php
 * @author     Samnan ur Rehman
 * @copyright  (c) 2008-2014 Samnan ur Rehman
 * @web        http://mywebsql.net
 * @license    http://mywebsql.net/license
 */ 
class MyWebSQL_Authentication  {
 
   const AUTH_TYPE_NONE ="NONE";
   const AUTH_TYPE_LOGIN ="LOGIN";
   public $username, $error;
    public function authenticate() {
            
        include_once(BASE_PATH . '/lib/db/manager.php');
       //  include_once(BASE_PATH .'/config/auth.php');
        
        $this->db = new DbManager();
        $this->error = ''; 
        
         // change of auth type at runtime invalidates session
        if (Session::get('auth', 'type') != $_SESSION["AUTH_TYPE"])
            Session::del('auth', 'valid');

        if (Session::get('auth', 'valid'))  {
            return $this->setParameters();
        }
             
        if ($_SESSION["AUTH_TYPE"] == 'NONE')
            return $this->getAuthNone();
            
        else if ($_SESSION["AUTH_TYPE"] == 'LOGIN')  
            return $this->getAuthLogin();    
        
        else{
            $this->setError(__('Invalid server configuration'));
            return false;
        }
    }
    
    public function getUserName() {
       return $this->username;
    } 
    
    public function getError() {
        return $this->error;
    }
     
    private function setParameters() { 
        define("DB_HOST", session::get('auth', 'host', true));
        define("DB_PORT", session::get('db', 'port', true));
        define("DB_SOCKET", session::get('db', 'socket', true)); 
        define("DB_USER", session::get('auth', 'user', true));
        define("DB_PASS", session::get('auth', 'pwd', true));
        
        // set the language
        include(BASE_PATH . '/config/lang.php');
        if (isset($_REQUEST["lang"]) && array_key_exists($_REQUEST["lang"], $_LANGUAGES) && file_exists(BASE_PATH . '/lang/' . $_REQUEST["lang"] . '.php')) {
            $_lang = $_REQUEST["lang"];
            setcookie("lang", $_REQUEST["lang"], time() + (COOKIE_LIFETIME * 60 * 60), EXTERNAL_PATH);
        }

        return true;
    }

    private function createConstants($server){
        Session::set('auth', 'valid', true);
        Session::set('auth', 'server_name', $server->Friendly_name, true);
        Session::set('auth', 'host', $server->host, true);
        Session::set('db', 'driver', $server->driver);
        Session::set('db', 'port', $server->port, true);
        Session::set('db', 'socket', $server->socket, true);
    }
    
    private function getAuthNone() {
        $server = new ServerDetails();
        if ($this->db->connect($server, $server->Anon_username, $server->Anon_password))   {
            Session::set('auth', 'user', $server->Anon_username, true);
            Session::set('auth', 'pwd', $server->Anon_password, true);
             
            Session::set('auth', 'type', self::AUTH_TYPE_NONE); 
            $this->createConstants($server);
            $this->db->disconnect(); 
            $this->setParameters();
            return true;
         } else
               $this->error = $this->db->getError();
        
        return false;
    }
            
    private function getAuthLogin() {
        if (isset($_POST['auth_user']) && isset($_POST['auth_pwd'])) {
            $server = new ServerDetails();
            $this->username = $_POST['auth_user'];
            if ($this->db->connect($server, $_POST['auth_user'], $_POST['auth_pwd'])) {
                Session::set('auth', 'user', $_POST['auth_user'], true);
                Session::set('auth', 'pwd', $_POST['auth_pwd'], true);
              
                Session::set('auth', 'type', self::AUTH_TYPE_LOGIN); 
                $this->createConstants($server);
                $this->db->disconnect();
                $this->setParameters();
                header('Location: ' . EXTERNAL_PATH);
                return true;
            } else
                $this->error = $this->db->getError();
        } 
        return false;
    } 
}

?>
