<script type="text/javascript" language='javascript' src="https://<?php echo $_SERVER['HTTP_HOST']?>/cache.php?script=jquery"></script>
<div id="results">
	<div class="message ui-state-default"><?php echo __('New database successfully created'); ?> [ {{DB_NAME}} ]</div>
</div>

<script type="text/javascript" language='javascript'>
	parent.addCmdHistory("{{SQL}}");
	parent.transferResultMessage(-1, '{{TIME}}', '<?php echo __('New database successfully created'); ?>');
	if ({{REDIRECT}})
		parent.window.location = "https://<?php echo $_SERVER['HTTP_HOST']?>/".concat("{{DB_NAME}}");
	else {
        parent.$('#tablelist').append("<li><span class='odb'><a href='javascript:dbSelect(\"{{DB_NAME}}\")'>{{DB_NAME}}</a></span>");
		parent.$('#dblist').append('<option name="{{DB_NAME}}">{{DB_NAME}}</option>');
		var $r = parent.$("#dblist option");
		$r.sort(function(a, b) {
			if (a.text < b.text) return -1;
			if (a.text == b.text) return 0;
			return 1;
		});
		$($r).remove();
      parent.$("#dblist").append( $( $r ) );
	}
</script>
