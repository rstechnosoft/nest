
<link href='https://<?php echo $_SERVER['HTTP_HOST']?>/cache.php?css=theme,default,alerts,grid' rel="stylesheet" />

<style>
	#db_names option		{ padding: 3px 6px; }
</style>
 
<div id="popup_wrapper" style="display:none">
	<div id="popup_contents">
		<div class="input" id="button-list">
                        <div id="error_msg">
                        </div>
			<span><?php echo __('Select a User to Share DB'); ?>:</span><span><input type="text" name="userlist" id="userlist">
                        <input type='button' class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" id='btn_add_dbshare' value='<?php echo __('Add User'); ?>' /></span>
		</div>

		<div id="grid-tabs">
			<div id="grid-messages">{{MESSAGE}}</div>
			<ul>
			
				<li><a href="#tab-db"><?php echo __('Database Privileges'); ?></a></li>
			</ul>
			<div class="ui-corner-bottom">
				
				<div id="tab-db">
					&nbsp;
				</div>
			</div>
		</div>
	</div>

	<div id="popup_footer">
		<div id="popup_buttons">
                     <div  style="float:left"><input type='button' class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" id='btn_deln' value='<?php echo __('Delete User'); ?>' /></div>
			<div id="checkboxes" style="float:left"><input type="checkbox" name="selectall" id="selectall"><label class="right" for="selectall"><?php echo __('Select All/None'); ?></label></div>
                        <div style="float:right"><input type='button' id='btn_submit' value='<?php echo __('Share DB to User'); ?>' tabindex="1" /></div>
                       
                        <div  style="float:right"><input type='button' id='btn_cancel' value='<?php echo __('Cancel'); ?>' /></div>
			</div>
	</div>

</div>

<script type="text/javascript" language='javascript' src="https://<?php echo $_SERVER['HTTP_HOST']?>/cache.php?script=common,jquery,ui,editable,position,query,cookies,settings,alerts,users"></script>
<script type="text/javascript" language="javascript">
window.title = "<?php echo __('Database Share')?>";

var USERS = {{USERS}};
var USER_INFO = {{USER_INFO}};
var DATABASE = {{DATABASE}};
var PRIVILEGES = {{PRIVILEGES}};
var USER_PRIVILEGES = {{USER_PRIVILEGES}};
var PRIVILEGE_NAMES = {{PRIVILEGE_NAMES}};
var DB_PRIVILEGE_NAMES = {{DB_PRIVILEGE_NAMES}};

 function addUserForDBShare()
    {
        var username = $("#userlist").val();
        $.ajax({
            type: "POST",
            url: 'AjaxHelper.php',
            data: { func: 'isUserNameExists',  username : username },
            success: function(response) {
                if(response == 1)
                {
                    if(!(username in USER_PRIVILEGES))
                    {
                        $("#shareduser_names").append("<option style='font-weight: bold;' name='"+username+
                                "' value='"+username+"' class='used'>"+username+"</option>");

                        USER_PRIVILEGES[username] = [];
                    }
                    else
                    {
                        $("#error_msg").html("username already in list. please type another user");
                    } 
                     
                } 
                else
                { 
                    $("#error_msg").html(username +" username not exists");
                     return;
                }
            }
        });  
    }
    
$(function() {
	$('#grid-tabs').tabs({
		select: function(event, ui) {
			if (ui.index == 0)
				$('#checkboxes').hide();
			else if ( (ui.index == 2 && !$('#db_names').val()) )
				$('#checkboxes').hide();
			else {
				$('#checkboxes').show();
				cls = (ui.index==1) ? '#tab-global .prv' : '#tab-db .dbprv';
				$('#selectall').attr('checked', $(cls).not(':checked').length == 0);
			}
		} 
	});
	 
	$('#btn_del').button().click(deleteUser); 
        $('#btn_cancel').button().click(cancelOperation).hide();
	 
	$('#btn_submit').button().click(updateDB);
	$('#selectall').click(selectAll);
	 
	loadUserData("true");
});
</script>