<?php
/**
 * This file is a part of MyWebSQL package
 *
 * @file:      modules/usermanager.php
 * @author     Samnan ur Rehman
 * @copyright  (c) 2008-2014 Samnan ur Rehman
 * @web        http://mywebsql.net
 * @license    http://mywebsql.net/license
 */

	function processRequest(&$db) {
		$action = v($_REQUEST["id"]);
		include(BASE_PATH . "/lib/usermanager.php");
		$legacyServer = Session::get('db', 'version') < 5;
		$editor = new userManager($db, $legacyServer);
		$message = '';

		if ($action != '') {
			 if ($action == "update")
				$result = updateUser($db, v($_REQUEST["query"]), $editor);

			if ($result) {
				$db->flush('PRIVILEGES', true);
				$message = __('The command executed successfully');
			}
			else
				$message = __('Error occurred while executing the query');
		}

		displayUserForm($db, $editor, $message, $action);
	}

	function displayUserForm(&$db, &$editor, $message, $action) {
		 
                $databasename = session::get('db', 'name');
                // current user name is not plaintext in case of 'update' action
		$userName = session::get('auth', 'user', true);
                $host  = session::get('auth', 'host', true);
                if(!isset($databasename))
                {
                    echo view('session_expired');
                    return;
                }
                
                if(!$db->IsUserDBOwner($databasename, $userName, $host) )
                {
                    echo view('CannotShareDB');
                    return;
                }
                
                
		$userList = $editor->getSharedUsersList($databasename);
		$privilegeNames = Privileges::getNames();
		$dbPrivilegeNames = DbPrivileges::getNames();
 
		$currentUser = selectUser($userList, $userName);
		$privileges  = array();
		$userPrivileges = array();
		$userInfo = array();
		if ($currentUser) {
			$privileges = $currentUser->getGlobalPrivileges();
			foreach ($userList as $user_name) {
                            $userPrivileges[$user_name->userName] = $user_name->getDbPrivileges($databasename);
                        }

                    $userInfo = array('username' => $currentUser->userName);
		} 

		$replace = array(
			'ID' => v($_REQUEST["id"]) ? htmlspecialchars($_REQUEST["id"]) : '',
			'MESSAGE' => $message,
			'USERS' => json_encode($userList),
			'USER_INFO' => json_encode($userInfo),
			'DATABASE' => json_encode($databasename),
			'PRIVILEGES' => json_encode($privileges),
			'USER_PRIVILEGES' => count($userPrivileges) >0 ? json_encode($userPrivileges): json_encode(new stdClass()),
			'PRIVILEGE_NAMES' => json_encode($privilegeNames),
			'DB_PRIVILEGE_NAMES' => json_encode($dbPrivilegeNames)
		);
		
                echo view('dbshare', $replace);
	}

	function selectUser($list, $user) {
		foreach($list as $obj) {
			$name = $obj->userName . '@' . $obj->host;
			if ($user == $name)
				return $obj;
		}

		$obj = count($list) > 0 ? $list[0] : NULL;
		return $obj;
	}

 
	function updateUser(&$db, $info, &$editor) {
		$info = json_decode($info);
		if (!is_object($info))
			return false;
  
                $db_name = session::get('db', 'name');
		 
		foreach($info->user_privileges as $SharedUser => $prvileges) {
                        $user =  $editor->getUser($SharedUser, "localhost");
			$result = $user->setDbPrivileges($db_name, isset($prvileges) ?$prvileges : array() );
			if (!$result)
				return false;
		}
 
		return true;
	}
 
        