<?php
 
require_once BASE_PATH."/lib/util.php";
class MyWebSQL_Registration{
      private $userName, $error;
    //If successful registration returns true, otherewise false.
    public function register() {
        include(BASE_PATH . "/lib/usermanager.php");
        include_once(BASE_PATH . '/lib/db/manager.php');
        $this->db = new DbManager();
        $this->error = '';
        $this->username = '';
 
        return $this->getAuthRegister(); 
    }
    public function getUserName() {
       return $this->username;
    }
    
    public function getError() {
        return $this->error;
    }
    private function getAuthRegister() {

         if (isset($_REQUEST['username']) && isset($_REQUEST['userpass'])) {
            $this->username = $_REQUEST['username']; 
            
            $_db_info = getDBClass();
            include_once($_db_info[0]);
            $_db_class = $_db_info[1];
            $DB = new $_db_class();
            unset($_db_info);
            unset($_db_class);
            
            $this->db = $DB;
            $this->db->conn = $this->db->GetRootConnection();
            
            $usermanager = new UserManager($this->db);
            //Hostname is in $info['hostname'] must be 'localhost'
            $usermanager->add($this->username,'localhost' , $_REQUEST['userpass']);
            $this->db->conn = null;
            return true; 
        } 
        return false;                               
    }
            
}

?>
