<?php

define('BASE_PATH', dirname(__FILE__));

header("Content-Type: text/html;charset=utf-8");
include_once(BASE_PATH . "/lib/session.php");
Session::init();

date_default_timezone_set('UTC');
include(BASE_PATH . '/modules/configuration.php');
initConfiguration();

// buffer unless we are in the download module (it will handle the buffering itself)
if (v($_REQUEST["type"]) != "dl") {
    include_once(BASE_PATH . "/lib/output.php");
    Output::buffer();
} else {
    $_REQUEST["type"] = 'download';
}

//check $_POST["auth_user"], auth_password, .... Variable available, then call usermanager.add
// require( BASE_PATH . '/modules/register.php');


if (defined("TRACE_FILEPATH") && TRACE_FILEPATH && defined("TRACE_MESSAGES") && TRACE_MESSAGES)
    ini_set("error_log", TRACE_FILEPATH);

include_once(BASE_PATH . "/lib/util.php");
require( BASE_PATH . '/modules/register.php');
$regi_module = new MyWebSQL_Registration();

if (!$regi_module->register()) {

    include(BASE_PATH . "/modules/splashr.php");
    $form = view('register', array(
        'LOGINID' => htmlspecialchars($regi_module->getUserName())));
    echo getSplashScreen($regi_module->getError(), $form);

    Output::flush();
    exit();
}
else
{
    //Redirect to registration success page.
    echo view('registrationsuccess' ); 
}
?>
