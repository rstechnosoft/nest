<?php
    if(!class_exists('ServerDetails'))
    {
        class ServerDetails
        {
             public $Root_username;
             public $Root_password;
             public $host;
             public $System_database;
             public $Version;
             public $driver;
             public $port;
             public $socket;
             public $Anon_username; 
             public $Anon_password;
             public $Friendly_name;
             
            public function __construct()
            {
                $strArray = explode(".", $_SERVER['HTTP_HOST']); 
                if (preg_replace('/[0-9]+/', '', $strArray[0]) == "mysql"){
                   $this->driver = 'mysqli';
                   Session::set('db', 'driver', $this->driver);
                   if(substr($strArray[0], -2) == 55) { 
                       $this->Root_username ='msandbox';
                       $this->Root_password ='msandbox';
                       $this->host ='localhost';
                       $this->System_database ='mysql';
                       $this->Version ='5.5'; 
                       $this->port ='5552'; 
                       $this->socket = '/tmp/mysql_sandbox5552.sock';
                       $this->Anon_username = 'msandbox';
                       $this->Anon_password = 'msandbox';
                       $this->Friendly_name = 'Shalin mysql 5.5 server';
                   }
                   else if(substr($strArray[0], -2) == 56) {
                       $this->Root_username ='msandbox';
                       $this->Root_password ='msandbox';
                       $this->host ='localhost';
                       $this->System_database ='mysql';
                       $this->Version ='5.6'; 
                       $this->port ='5629';  
                       $this->socket = "/tmp/mysql_sandbox5629.sock";
                       $this->Anon_username = 'test';
                       $this->Anon_password = 'test';
                       $this->Friendly_name = 'Shalin mysql 5.6 server';
                   }
                   else if(substr($strArray[0], -2) == 57) {
                       $this->Root_username ='root';
                       $this->Root_password ='root';
                       $this->host ='localhost';
                       $this->System_database ='mysql';
                       $this->Version ='5.7'; 
                       $this->port ='3307';
                       $this->socket = '/tmp/mysql_sandbox5629.sock';
                       $this->Anon_username = 'test';
                       $this->Anon_password = 'test';
                       $this->Friendly_name = 'Shalin mysql 5.7 server';
                   }
                   else {
                         $this->setError(__('Only supports MYSQL Version 5.5 to 5.7')); 
                    }
                }
                else {
                    $this->setError(__('The specified subdomain may not be available')); 
                } 
            }
        }
    }
        ?>