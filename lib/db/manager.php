<?php
/**
 * This file is a part of MyWebSQL package
 * Provides a generic wrapper for database connection functionality
 *
 * @file:      lib/db/manager.php
 * @author     Samnan ur Rehman
 * @copyright  (c) 2008-2014 Samnan ur Rehman
 * @web        http://mywebsql.net
 * @license    http://mywebsql.net/license
 */

if (defined("CLASS_COMMON_DB_INCLUDED"))
	return true;

define("CLASS_COMMON_DB_INCLUDED", "1");
require_once BASE_PATH."/lib/util.php";
class DbManager {
	var $conn;
	var $errMsg;

	function __construct() {
	}        
	function connect($server, $user, $password, $db="") {
                $host  = $server->host;
		$class = getDBClass($server->driver)[1];
                $port = $server->port;
                $socket = $server->socket;
                $db = new $class();
		$db->setAuthOptions($server);

		$result = $db->connect($host, $user, $password, '', $port, $socket);
		if (!$result) {
			$this->errMsg = $db->getError();
			return false;
		}
		$db->disconnect();
		return true;
	}
         
	// required for proper functionality
	function disconnect() {
	}

	function getError() {
		return $this->errMsg;
	}
}

function replace_single_quotes($s) {
	return str_replace("''", "'", $s);
}

?>