<?php
/**
 * This file is a part of MyWebSQL package
 * user classes used by user manager
 *
 * @file:      lib/user.php
 * @author     Ovais Tariq <http://ovaistariq.net>
 * @maintainer Samnan ur Rehman
 * @copyright  (c) 2008-2014 Samnan ur Rehman
 * @web        http://mywebsql.net
 * @license    http://mywebsql.net/license
 */

abstract class User {
	protected static $dbManager;

	public $userName;
	public $host;
	public $password;

	protected $globalPrivileges;
	protected $dbPrivileges;

	public static function setDb($dbManager) {
		self::$dbManager = $dbManager;

		Privileges::setDb( $dbManager );
	}

	public static function factory() {
		return new User_5x();
	}

	public function __construct() {
		$this->userName  = false;
		$this->host      = false;
		$this->password  = false;

		$this->globalPrivileges = false;
		$this->dbPrivileges     = array();
	}

	public abstract function add();

	public abstract function update($newUsername, $newHost = '%');

        public abstract function usernameexists();
        
	public function updatePassword($newPassword = '') {
		if( false == $this->userName || false == $this->host )
			return false;

		$userName = self::$dbManager->escape( $this->userName );
		$host     = self::$dbManager->escape( $this->host );
		$password = self::$dbManager->escape( $newPassword );

		$sql = "SET PASSWORD FOR '$userName'@'$host' = PASSWORD('$password')";

		if( false == self::$dbManager->query( $sql ) )
			return false;

		$this->password  = $newPassword;

		return true;
	}

	public function delete() {
		if( false == $this->userName || false == $this->host )
			return false;

		$userName = self::$dbManager->escape( $this->userName );
		$host     = self::$dbManager->escape( $this->host );

		$sql = "DROP USER '$userName'@'$host'";

		return self::$dbManager->query( $sql );
	}

	public function getGlobalPrivileges() {
		if( false == $this->globalPrivileges )
			$this->globalPrivileges = new Privileges( $this->userName, $this->host );

		return $this->globalPrivileges->get();
	}

	public function setGlobalPrivileges($privileges) {
		if( false == $this->globalPrivileges )
			$this->globalPrivileges = new Privileges( $this->userName, $this->host );

		return $this->globalPrivileges->set( $privileges );
	}

	public function getDbPrivileges($dbName) {
		$dbName = trim( $dbName );
		if( false == isset( $this->dbPrivileges[$dbName] ) )
			$this->dbPrivileges[$dbName] = new DbPrivileges( $this->userName, $this->host, $dbName );

		return $this->dbPrivileges[$dbName]->get();
	}

	public function setDbPrivileges($dbName, $privileges) {
		$dbName = trim( $dbName );
		if( false == isset( $this->dbPrivileges[$dbName] ) )
			$this->dbPrivileges[$dbName] = new DbPrivileges( $this->userName, $this->host, $dbName );

		return $this->dbPrivileges[$dbName]->set( $privileges );
	}
}


/**
 * The user class for MySQL version 5.x
 */
class User_5x extends User {
	public function  __construct() {
		parent::__construct();
	}

	public function add() {
		if( false == $this->userName || false == $this->host )
			return false;

		$userName = self::$dbManager->escape( $this->userName );
		$host     = self::$dbManager->escape( $this->host );
		$password = self::$dbManager->escape( $this->password );

		$sql = "CREATE USER '$userName'@'$host' IDENTIFIED BY '$password'";

		return self::$dbManager->query( $sql );
	}

        public function usernameexists()
        { 
            $_db_info = getDBClass();
            if ($_db_info != null) {
                include_once($_db_info[0]);
                $_db_class = $_db_info[1];
                $DB = new $_db_class();
                unset($_db_info);
                unset($_db_class);
            }
            $connect = $DB->getRootConnection();
	 
            if ($connect->connect_errno){
                return $this->error(__('Database connection failed to the server')); 
            }
            $user_check = $connect->prepare("SELECT User FROM mysql.user WHERE User= ? ");

            $user_check->bind_param("s", $this->userName);
            $user_check->execute();
            $user_check->store_result();
            $do_user_check = $user_check->num_rows;
            $user_check->free_result();
            $user_check->close();
            return ($do_user_check > 0);
        }
        
      
	public function update($newUsername, $newHost = '%') {
		$newUsername = self::$dbManager->escape( $newUsername );
		$newHost     = self::$dbManager->escape( $newHost );

		if( false == $newUsername || false == $newHost )
			return false;

		$sql = "RENAME USER '{$this->userName}'@'{$this->host}' TO '$newUsername'@'$newHost'";

		if( false == self::$dbManager->query( $sql ) )
			return false;

		$this->userName  = $newUsername;
		$this->host      = $newHost;

		return true;
	}
}