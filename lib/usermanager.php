<?php
/**
 * This file is a part of MyWebSQL package
 * library classes to manage mysql users and permissions
 *
 * @file:      lib/usermanager.php
 * @author     Ovais Tariq <http://ovaistariq.net>
 * @maintainer Samnan ur Rehman
 * @copyright  (c) 2008-2014 Samnan ur Rehman
 * @web        http://mywebsql.net
 * @license    http://mywebsql.net/license
 */    
include_once 'privileges.php';
include_once 'user.php';

class UserManager {
	protected $db;  

	public function __construct( $db ) {
		$this->db     = $db;
 
		User::setDb( $this->db );
	}

        public function usernameexists($userName) {
            $user = User::factory(  ); 
            $user->userName = $userName; 
            return $user->usernameexists();
        }
        
	public function getUser($userName, $host = '%') {
		$user = User::factory(  );
		$user->userName = $userName;
		$user->host     = $host;

		return $user;
	}

	public function add($userName, $host = '%', $password = '') {
		$user = User::factory(  );

		$user->userName = $userName;
		$user->host     = $host;
		$user->password = $password;

		return $user->add();
	}

	public function update($currUsername, $currHost, $newUsername, $newHost) {
		$user = User::factory(  );

		$user->userName = $currUsername;
		$user->host     = $currHost;

		return $user->update( $newUsername, $newHost );
	}

	public function updatePassword($userName, $host, $newPassword) {
		$user = User::factory(  );

		$user->userName = $userName;
		$user->host     = $host;

		return $user->updatePassword( $newPassword );
	}

	public function delete($userName, $host) {
		$user = User::factory(  );

		$user->userName = $userName;
		$user->host     = $host;

		return $user->delete();
	}

	public function getUsersList() {
		$tblName = Privileges::$privilegesTable;

		$sql = "SELECT `User`, `Host`, `Password` FROM $tblName ORDER BY `User`, `Host`";

		if( false == $this->db->query( $sql ) )
			return array();

		$users = array();
		while( $row = $this->db->fetchRow() ) {
			$user = User::factory(  );

			$user->userName = $row['User'];
			$user->host     = $row['Host'];
			$user->password = $row['Password'];

			$users[] = $user;
		}

		return $users;
	}
        public function getsharedUsersList($databasename) {
            $sharedusers = array();
            $this->db->oldConn = $this->db->conn;       
            $this->db->conn = $this->db->GetRootConnection();
             $sql =  "SELECT DISTINCT db.User, db.Host FROM `mysql`.`db` where `mysql`.`db`.`db` ='$databasename'";  
           //   $sql = "SELECT DISTINCT user.User, Password, user.Host FROM `mysql`.`db` inner join `mysql`.`user` on db.User = user.User where `mysql`.`db`.`db` = '$databasename'"; 
              
              if( false == $this->db->query( $sql ) )
			return array();

		 
		while( $row = $this->db->fetchRow() ) {
			$user = User::factory(  );

			$user->userName = $row['User'];
			$user->host     = $row['Host']; 

			$sharedusers[] = $user;
		}
                $this->db->conn = $this->db->oldConn;       
		return $sharedusers;
        }
}